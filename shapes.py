import math
from abc import abstractmethod


class Shape:
    @abstractmethod
    def area(self):
        pass


class Circle(Shape):
    def __init__(self, r):
        self.radius = r

    def area(self):
        return math.pi * (self.radius ** 2)


class Square(Shape):
    def __init__(self, a):
        self.width = a

    def area(self):
        return self.width ** 2

    def preme(self):
        return self.width * 4


class Rectangle(Shape):
    def __init__(self, a, b):
        self.width = a
        self.length = b

    def area(self):
        return self.width * self.length
