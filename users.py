class Person:
    def __init__(self, first_name, last_name, *args, **kwargs):
        self.first_name = first_name
        self.last_name = last_name

    def __str__(self):
        return 'Person: {}'.format(self.show_fullname())

    def show_fullname(self):
        return "{} {}".format(self.first_name, self.last_name)


class Student(Person):
    """

    """
    students = []

    def __init__(self, sid=0, *args, **kwargs):
        self.student_id = sid
        self.scores = []
        super().__init__(*args, **kwargs)
        self.students.append(self)

    def __str__(self):
        return "Student: {}, SID: {}".format(
            self.show_fullname(), self.student_id
        )


class Teacher(Person):
    def __init__(self, tid, *args, **kwargs):
        self.teacher_id = tid
        super().__init__(*args, **kwargs)

    def __str__(self):
        return super().__str__()


class BaseUser:
    def __init__(self, first_name, last_name, age, *args, **kwargs):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        super().__init__(*args, **kwargs)

    def title(self):
        return "User: {}".format(self.first_name)


class Device:
    devices_list = []

    def __init__(self, model, os_version, os_name, uuid,*args, **kwargs):
        self.device_model = model
        self.os_version = os_version
        self.os_name = os_name
        self.uuid = uuid
        self._private_uuid = self.uuid + 'p'
        self.__internal_uuid = self._private_uuid + 'I'
        Device.devices_list.append(self)
        super().__init__(*args, **kwargs)

    def title(self):
        return f"Device: {self.device_model}"

    def internal_uuid(self):
        return self.__internal_uuid

    def __repr__(self):
        return self.uuid

    @staticmethod
    def __check_existence(uuid, device_list):
        exists = None
        for device in device_list:
            if uuid == device.uuid:
                exists = device
        return exists

    @classmethod
    def create(cls, uuid, **kwargs):
        exists = cls.__check_existence(uuid, cls.devices_list)
        if exists is None:
            exists = cls(uuid=uuid, **kwargs)
        return exists

    def newer(self, device_2):
        return self if int(self.os_version) > int(device_2.os_version) else device_2


class MobileUser(BaseUser):
    users_list = []

    def __init__(self, phone_number, device,*args, **kwargs):
        self.phone_number = phone_number
        self.device = device
        super().__init__(*args, **kwargs)

    def __str__(self):
        return str(self.phone_number)

    def __eq__(self, other):
        return self.phone_number == other.phone_number


class WebUser(BaseUser):
    def __init__(self, email, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.email = email


class Test:
    pass