from time import sleep

from decorators import log_time
from shapes import Circle, Square
from users import Student, Person, Teacher, WebUser, MobileUser, Device


@log_time
def high_load(n):
    sleep(n)
    return "Done!"


if __name__ == '__main__':
    # my_device = Device.create(
    #     model='GALAXY-S10', os_version='9', os_name='NOQA', uuid='123'
    # )
    # my_device2 = Device.create(
    #     model='GALAXY-S10', os_version='10', os_name='OREO', uuid='456'
    # )
    # print(my_device._private_uuid)
    # print(my_device.__internal_uuid)
    # # newer_device = my_device.newer(my_device2)
    # user = MobileUser(
    #     phone_number=98123456789, first_name='ali',
    #     last_name='razavi', age=22, device=my_device
    # )
    # user2 = MobileUser(
    #     phone_number=98123456789, first_name='reza',
    #     last_name='alavi', age=32, device=my_device2
    # )
    # print(user)
    print(high_load(13))
    # print(high_load(2))
